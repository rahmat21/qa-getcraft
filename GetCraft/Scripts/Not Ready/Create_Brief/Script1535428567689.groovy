import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int RN

RN = ((Math.random() * 500) as int)

String name_project = GlobalVariable.project_name + RN

not_run: WebUI.callTestCase(findTestCase('GETCRAFT/Register/Register_Client'), [('email') : 'Testing_Client', ('domain') : '@mailinator.com'
        , ('name') : 'Testing', ('name2') : 'Client', ('phonenumber') : '087788334', ('password') : 'getcraft1234'], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(5)

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://staging.getcraft.io/signin')

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), ' ')

WebUI.setText(findTestObject('GETCRAFT/login/input_username'), 'qa_test_email+9270@getcraft.com')

WebUI.click(findTestObject('Object Repository/GETCRAFT/login/button_signin'))

WebUI.verifyElementPresent(findTestObject('GETCRAFT/login/present_myproject'), 121)

WebUI.click(findTestObject('GETCRAFT/create_brief/Browse creators'))

WebUI.click(findTestObject('GETCRAFT/create_brief/p_Celebrities  Influencers'))

not_run: WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

not_run: WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('GETCRAFT/create_brief/Browse creators'))

not_run: WebUI.click(findTestObject('GETCRAFT/create_brief/p_Celebrities  Influencers'))

not_run: WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/button_search'), 500)

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/checklist_service'), 300)

WebUI.click(findTestObject('GETCRAFT/create_brief/checklist_service2'))

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Review and request a qu'))

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Request a quote'))

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_projectTitle'), name_project)

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_brandName'), 'Getcraft')

WebUI.setText(findTestObject('GC2/Page_GetCraft - Content Made Effici/input_desc'), 'tes description')

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_audience'), 'Milenials')

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/input_audience'), 100)

not_run: CustomKeywords.'uploadFiles.uploadFile'(findTestObject('GETCRAFT/create_brief/Upload Reference'), '/Users/rachmat/Documents/jiren.jpg')

not_run: Thread.sleep(2000)

not_run: WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/submit_reference'), 500)

not_run: WebUI.click(findTestObject('GETCRAFT/create_brief/submit_reference'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/create_brief/checklist_buildbrand'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/create_brief/project_budget'), '100000')

WebUI.click(findTestObject('GETCRAFT/create_brief/div'))

WebUI.click(findTestObject('GETCRAFT/create_brief/payment_100 Upfront'))

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/button_Send to Creator'), 100)

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Send to Creator'))

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Send to Creator2'))

WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/button_Send to Creator3'), 120)

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Send to Creator3'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/login/present_myproject'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('GETCRAFT/create_brief/verify_projectname'), 120)

WebUI.verifyElementPresent(findTestObject('GETCRAFT/create_brief/verify_invitation'), 100)

WebUI.delay(3)

not_run: WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

