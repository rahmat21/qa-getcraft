import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

int RN

RN = ((Math.random() * 599) as int)

String mailinator = email + RN

String user = (email + RN) + domain

String firstname = name + RN

String lastname = name2 + RN

String Phone = phonenumber + RN

String name_project = GlobalVariable.project_name + RN

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.click(findTestObject('GETCRAFT/brief_phase2/request_QUOTE'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/brief_phase2/Influencer'))

WebUI.click(findTestObject('GETCRAFT/create_brief/sub_service'))

not_run: WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/service_artikel'), 120)

WebUI.click(findTestObject('GETCRAFT/brief_phase2/country_selectall'))

WebUI.click(findTestObject('GETCRAFT/create_brief/select_subservice'), FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/sub_service'), 20)

WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/country'), 120)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/country'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/country_indonesia'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/select_subservice'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/industry_topic'), 120)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/industry_topic'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/Selectall_industry'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GETCRAFT/create_brief/select_subservice'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_projectTitle'), name_project)

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_brandName'), 'Getcraft')

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_desc'), 'tes description')

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_audience'), 'Milenials')

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/checklist_buildbrand'), 99)

WebUI.click(findTestObject('GETCRAFT/create_brief/checklist_buildbrand'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/create_brief/project_budget'), '100000')

WebUI.click(findTestObject('GETCRAFT/create_brief/div'))

WebUI.click(findTestObject('GETCRAFT/create_brief/payment_100 Upfront'))

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/button_Save as Draft'), 100)

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Save as Draft'))

WebUI.waitForElementPresent(findTestObject('GETCRAFT/brief_phase2/input_lastName'), 120)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/input_firstName'), firstname)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/input_lastName'), lastname)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/input_email'), user)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/input_password'), password)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/input_phoneNumber'), Phone)

not_run: WebUI.setText(findTestObject('GETCRAFT/brief_phase2/getcraftID'), firstname)

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('GETCRAFT/brief_phase2/select_country'))

not_run: WebUI.click(findTestObject('GETCRAFT/register/select_indonesia'))

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('GETCRAFT/brief_phase2/select_city'))

not_run: WebUI.waitForElementClickable(findTestObject('GETCRAFT/register/select_jakarta'), 500)

not_run: WebUI.click(findTestObject('GETCRAFT/register/select_jakarta'))

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('GETCRAFT/brief_phase2/select_client'))

not_run: WebUI.delay(5)

WebUI.scrollToElement(findTestObject('GETCRAFT/brief_phase2/sign-in'), 100)

not_run: WebUI.mouseOver(findTestObject('GETCRAFT/brief_phase2/checkbo'))

WebUI.delay(10)

not_run: CustomKeywords.'JSclick.clickUsingJS'(findTestObject('GETCRAFT/brief_phase2/checkbo'), 30)

not_run: CustomKeywords.'JSclick.clickUsingJS'(findTestObject('GETCRAFT/brief_phase2/checkbo'), 30)

not_run: WebElement element = WebUiCommonHelper.findWebElement(findTestObject('GETCRAFT/brief_phase2/checkbo'), 30)

not_run: WebUI.executeJavaScript('arguments[0].click', Arrays.asList(element))

WebUI.click(findTestObject('GETCRAFT/brief_phase2/checkbo'))

not_run: CustomKeywords.'JSclick.clickUsingJS'(findTestObject('GETCRAFT/brief_phase2/checkbo'), 10)

not_run: WebUI.scrollToElement(findTestObject('GETCRAFT/brief_phase2/sign-in'), 100)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('GETCRAFT/brief_phase2/sign-in'))

not_run: WebUI.setText(findTestObject('GETCRAFT/login/input_username'), 'Testing_Client28@mailinator.com')

not_run: WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), 'getcraft1234')

not_run: WebUI.delay(5)

not_run: WebUI.click(findTestObject('GETCRAFT/brief_phase2/button_signin'))

not_run: WebUI.waitForElementPresent(findTestObject('GETCRAFT/create_brief/verify_projectname'), 120)

not_run: WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.verifyElementText(findTestObject('Object Repository/GETCRAFT/create_brief/verify_projectname'), name_project)

not_run: WebUI.verifyElementPresent(findTestObject('GETCRAFT/create_brief/verify_draft'), 100)

not_run: WebUI.delay(3)

not_run: WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

