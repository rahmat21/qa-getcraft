import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

int RN

RN = ((Math.random() * 599) as int)

String name_project = GlobalVariable.project_name + RN

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging_login)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.setText(findTestObject('GETCRAFT/login/input_username'), 'qa_test_email+9251@getcraft.com')

WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), '1234567890')

WebUI.click(findTestObject('Object Repository/GETCRAFT/login/button_signin'))

WebUI.click(findTestObject('GETCRAFT/login/present_myproject'))

WebUI.click(findTestObject('GETCRAFT/submit_pitch/select_project'))

WebUI.waitForElementClickable(findTestObject('GETCRAFT/submit_pitch/submitted_pitch'), 10)

WebUI.delay(10)

WebUI.click(findTestObject('GETCRAFT/submit_pitch/submitted_pitch'))

WebUI.waitForElementPresent(findTestObject('GETCRAFT/submit_pitch/add_pitch'), 10)

WebUI.delay(6)

WebUI.click(findTestObject('GETCRAFT/submit_pitch/add_pitch'))

WebUI.click(findTestObject('GETCRAFT/submit_pitch/creatif_idea'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/submit_pitch/creatif_idea'), 'I have to submit pitch with automation running' + RN)

WebUI.scrollToPosition(0, 800)

WebUI.delay(6)

not_run: WebUI.click(findTestObject('GETCRAFT/submit_pitch/price2'))

WebUI.setText(findTestObject('GETCRAFT/submit_pitch/price2'), '450000')

WebUI.setText(findTestObject('GETCRAFT/submit_pitch/price'), '2')

WebUI.scrollToPosition(0, 800)

WebUI.setText(findTestObject('GETCRAFT/submit_pitch/views'), '100')

not_run: WebUI.mouseOver(findTestObject('GETCRAFT/submit_pitch/price2'))

not_run: CustomKeywords.'uploadFiles.price'()

not_run: WebUI.click(findTestObject('GETCRAFT/submit_pitch/viewer'))

not_run: WebUI.mouseOver(findTestObject('GETCRAFT/submit_pitch/viewer'))

not_run: CustomKeywords.'uploadFiles.performance'()

WebUI.scrollToElement(findTestObject('GETCRAFT/submit_pitch/submit_pitch'), 300)

WebUI.click(findTestObject('GETCRAFT/submit_pitch/submit_pitch'))

WebUI.verifyElementPresent(findTestObject('GETCRAFT/submit_pitch/idea_review'), 120)

