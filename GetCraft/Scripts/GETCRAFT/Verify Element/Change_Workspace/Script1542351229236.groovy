import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int RN

RN = ((Math.random() * 599) as int)

String name_project = GlobalVariable.project_name + RN

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging_login)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.setText(findTestObject('GETCRAFT/login/input_username'), GlobalVariable.Staging_UserMultiple)

WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), GlobalVariable.Staging_PassMultiple)

WebUI.click(findTestObject('Object Repository/GETCRAFT/login/button_signin'))

WebUI.verifyElementPresent(findTestObject('GETCRAFT/login/present_myproject'), 120)

WebUI.click(findTestObject('GETCRAFT/general/dropdown_menu'))

text = WebUI.getText(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.click(findTestObject('GETCRAFT/general/select_workspace'))

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('GETCRAFT/general/workspace_name'), text)

WebUI.delay(3)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

