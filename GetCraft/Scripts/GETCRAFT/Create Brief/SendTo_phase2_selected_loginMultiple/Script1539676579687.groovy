import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int RN

RN = ((Math.random() * 599) as int)

String name_project = GlobalVariable.project_name + RN

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.click(findTestObject('GETCRAFT/create_brief/p_Celebrities  Influencers'))

WebUI.delay(4)

WebUI.waitForElementClickable(findTestObject('GETCRAFT/create_brief/button_search'), 120)

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/button_search'), 100)

WebUI.click(findTestObject('GETCRAFT/create_brief/checklist_service2'))

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Review and request a qu'))

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Request a quote'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_projectTitle'), name_project)

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_brandName'), 'Getcraft')

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_desc'), 'tes description')

WebUI.setText(findTestObject('GETCRAFT/create_brief/input_audience'), 'Milenials')

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/checklist_buildbrand'), 99)

WebUI.click(findTestObject('GETCRAFT/create_brief/checklist_buildbrand'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GETCRAFT/create_brief/project_budget'), '100000')

WebUI.click(findTestObject('GETCRAFT/create_brief/div'))

WebUI.click(findTestObject('GETCRAFT/create_brief/payment_100 Upfront'))

WebUI.scrollToElement(findTestObject('GETCRAFT/create_brief/button_Send to Creator'), 100)

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Send to Creator'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('GETCRAFT/brief_phase2/sign-in'), 100)

WebUI.delay(3)

WebUI.click(findTestObject('GETCRAFT/brief_phase2/sign-in'))

WebUI.setText(findTestObject('GETCRAFT/login/input_username'), GlobalVariable.Staging_UserMultiple)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), GlobalVariable.Staging_PassMultiple)

WebUI.delay(5)

WebUI.click(findTestObject('GETCRAFT/brief_phase2/button_signin'))

WebUI.waitForElementPresent(findTestObject('GETCRAFT/brief_phase2/workspace_chicknik'), 90)

WebUI.click(findTestObject('GETCRAFT/brief_phase2/workspace_chicknik'))

WebUI.click(findTestObject('GETCRAFT/brief_phase2/continue'))

WebUI.waitForElementPresent(findTestObject('GETCRAFT/login/present_myproject'), 120)

WebUI.click(findTestObject('GETCRAFT/create_brief/button_Send to Creator3'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('GETCRAFT/submit_pitch/submitted_pitch'), 120)

WebUI.delay(5)

WebUI.click(findTestObject('GETCRAFT/login/present_myproject'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('GETCRAFT/create_brief/verify_projectname'), 120)

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('Object Repository/GETCRAFT/create_brief/verify_projectname'), name_project)

WebUI.verifyElementPresent(findTestObject('GETCRAFT/create_brief/verify_invitation'), 360)

WebUI.closeBrowser()

