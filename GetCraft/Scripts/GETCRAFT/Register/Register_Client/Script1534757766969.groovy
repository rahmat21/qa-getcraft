import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

int RN

RN = ((Math.random() * 999) as int)

String mailinator = email + RN

String user = (email + RN) + domain

String firstname = name + RN

String lastname = name2 + RN

String Phone = phonenumber + RN

println(user)

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.waitForElementClickable(findTestObject('GETCRAFT/register/button_Daftar'), 180)

WebUI.click(findTestObject('GETCRAFT/register/button_Daftar'))

WebUI.setText(findTestObject('GETCRAFT/register/input_firstName'), firstname)

WebUI.setText(findTestObject('GETCRAFT/register/input_lastName'), lastname)

WebUI.setText(findTestObject('GETCRAFT/register/input_phoneNumber'), Phone)

WebUI.setText(findTestObject('GETCRAFT/register/input_email'), user)

WebUI.setText(findTestObject('GETCRAFT/register/input_password'), password)

WebUI.setText(findTestObject('GETCRAFT/register/input_ID'), firstname)

not_run: WebUI.click(findTestObject('GETCRAFT/register/button_Continue'))

not_run: WebUI.setText(findTestObject('GETCRAFT/register/input_name'), firstname)

WebUI.delay(5)

WebUI.click(findTestObject('GETCRAFT/register/select_country'))

WebUI.click(findTestObject('GETCRAFT/register/select_indonesia'))

WebUI.delay(5)

WebUI.click(findTestObject('GETCRAFT/register/select_city'))

WebUI.waitForElementClickable(findTestObject('GETCRAFT/register/select_jakarta'), 500)

WebUI.delay(5)

WebUI.click(findTestObject('GETCRAFT/register/select_jakarta'))

WebUI.delay(4)

WebUI.click(findTestObject('GETCRAFT/register/select_BoxClient'))

WebUI.click(findTestObject('GETCRAFT/register/button_Continue'))

WebUI.delay(6)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('http://www.mytrashmail.com/')

WebUI.setText(findTestObject('GETCRAFT/register/input_mailtrash'), mailinator)

WebUI.click(findTestObject('GETCRAFT/register/get_mailtrash'))

WebUI.click(findTestObject('GETCRAFT/register/open_email'))

not_run: WebUI.waitForElementPresent(findTestObject('GETCRAFT/register/link_verifikasi'), 60)

WebUI.verifyElementVisible(findTestObject('GETCRAFT/register/link_verifikasi'))

WebUI.click(findTestObject('GETCRAFT/register/link_verifikasi'))

WebUI.delay(9, FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.Staging_login)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/GETCRAFT/create_brief/Language_indo'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('GETCRAFT/login/Select_language'))

    WebUI.click(findTestObject('GETCRAFT/login/english_language'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(3)
} else {
    WebUI.delay(3)
}

WebUI.waitForElementClickable(findTestObject('GETCRAFT/login/button_signin'), 180)

WebUI.setText(findTestObject('Object Repository/GETCRAFT/login/input_password'), password)

WebUI.setText(findTestObject('GETCRAFT/login/input_username'), user)

WebUI.click(findTestObject('Object Repository/GETCRAFT/login/button_signin'))

WebUI.verifyElementPresent(findTestObject('GETCRAFT/login/present_myproject'), 120)

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

