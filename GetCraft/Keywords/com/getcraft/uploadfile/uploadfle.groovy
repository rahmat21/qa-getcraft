

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
//Added for use with "uploadFiles"
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.awt.AWTException
import java.awt.event.InputEvent
import java.awt.event.MouseEvent
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

import org.junit.After

//Used when "Upload File" or "Send Keys" will not work
public class uploadFiles {
	@Keyword
	def uploadFile (TestObject to, String filePath) {
		WebUI.click(to)
		StringSelection ss = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		//Open Goto window
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

		robot.delay(2000)

		robot.keyPress(KeyEvent.VK_META);
		robot.keyPress(KeyEvent.VK_SHIFT);
		robot.keyPress(KeyEvent.VK_G);


		robot.keyRelease(KeyEvent.VK_META);
		robot.keyRelease(KeyEvent.VK_SHIFT);
		robot.keyRelease(KeyEvent.VK_G);

		//Paste the clipboard value
		robot.keyPress(KeyEvent.VK_META);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_META);
		robot.keyRelease(KeyEvent.VK_V);
		robot.delay(2000)
		//Press Enter key to close the Goto window and Upload window
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}
	@Keyword
	def price() {
		Robot robot2 = new Robot();
		//tab
		/*
		 robot2.keyPress(KeyEvent.VK_TAB);
		 robot2.keyRelease(KeyEvent.VK_TAB);
		 robot2.keyPress(KeyEvent.VK_1);
		 robot2.keyRelease(KeyEvent.VK_1);
		 robot2.keyPress(KeyEvent.VK_TAB);
		 robot2.keyRelease(KeyEvent.VK_TAB);
		 */ 
		robot2.mousePress(InputEvent.BUTTON1_MASK);
		robot2.mouseRelease(InputEvent.BUTTON1_MASK);
		robot2.keyPress(KeyEvent.VK_1);
		robot2.keyRelease(KeyEvent.VK_1);
		robot2.keyPress(KeyEvent.VK_5);
		robot2.keyRelease(KeyEvent.VK_5);

		robot2.keyPress(KeyEvent.VK_0);
		robot2.keyRelease(KeyEvent.VK_0);
		robot2.keyPress(KeyEvent.VK_0);
		robot2.keyRelease(KeyEvent.VK_0);
		robot2.keyPress(KeyEvent.VK_0);
		robot2.keyRelease(KeyEvent.VK_0);
		robot2.keyPress(KeyEvent.VK_0);
		robot2.keyRelease(KeyEvent.VK_0);
		robot2.keyPress(KeyEvent.VK_0);
		robot2.keyRelease(KeyEvent.VK_0);

		robot2.delay(2000)

	}

	@Keyword
	def performance() {
		Robot robot3 = new Robot();
		//tab
		/*
		 robot3.keyPress(KeyEvent.VK_TAB);
		 robot3.keyRelease(KeyEvent.VK_TAB);
		 robot3.keyPress(KeyEvent.VK_1);
		 robot3.keyRelease(KeyEvent.VK_1);
		 robot3.keyPress(KeyEvent.VK_TAB);
		 robot3.keyRelease(KeyEvent.VK_TAB);
		 */
		robot3.mousePress(InputEvent.BUTTON1_MASK);
		robot3.mouseRelease(InputEvent.BUTTON1_MASK);
		robot3.keyPress(KeyEvent.VK_1);
		robot3.keyRelease(KeyEvent.VK_1);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.keyPress(KeyEvent.VK_0);
		robot3.keyRelease(KeyEvent.VK_0);
		robot3.delay(2000)

	}

	@Keyword
	def clickUsingJS(TestObject to, int timeout) {
		try {
			WebUI.click(to)
		}
		catch (Exception e) {
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement element = WebUiCommonHelper.findWebElement(to, timeout)
			JavascriptExecutor executor = ((driver) as JavascriptExecutor)
			executor.executeScript('arguments[0].click()', element)
		}
		throw(e)
	}

}
