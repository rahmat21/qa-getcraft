<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_id(app)div1div2headerclass</name>
   <tag></tag>
   <elementGuidId>b5ed659a-fb1d-4829-b98f-63fd7223217e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>katalon-rec_elementInfoDiv</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[2]/header[@class=&quot;page-header guest navbar navbar-fixed-top index__header__26T6B&quot;]/div[@class=&quot;page-header-inner container page-top index__topFixed__2IL1o&quot;]/div[@class=&quot;page-top nav hidden-xs hidden-sm&quot;]/div[@class=&quot;top-menu&quot;]/ul[@class=&quot;nav navbar-nav pull-right index__nav__WOgeF&quot;]/li[3]/a[@class=&quot;index__guestFont__OzbJ_ color-gc-blue&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;katalon-rec_elementInfoDiv&quot;)</value>
   </webElementProperties>
</WebElementEntity>
