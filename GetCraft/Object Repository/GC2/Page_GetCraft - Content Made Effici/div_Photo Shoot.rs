<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Photo Shoot</name>
   <tag></tag>
   <elementGuidId>5513fe79-ec55-4075-aca1-d2de2b49e97e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Photo Shoot</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[1]/div[@class=&quot;index__subHeaderFixed__2zrjw index__subHeaderFixed__2zrjw&quot;]/div[@class=&quot;container no-padding-xs&quot;]/div[@class=&quot;col-md-12 col-xs-12 col-sm-12 no-space&quot;]/div[@class=&quot;hidden-xs hidden-sm col-md-1 index__leftSideAllServices__3NR85&quot;]/div[@class=&quot;index__dropdownServices__YkE66 dropdown open btn-group&quot;]/ul[@class=&quot;left-services dropdown-menu&quot;]/div[1]/li[2]/span[@class=&quot;index__activeFormat__2Y2rO&quot;]/ul[@class=&quot;dropdown-menu index__actived__3IMzr&quot;]/li[@class=&quot;index__activeName__2qdeO&quot;]/div[1]/button[1]/div[1]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
