<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>5253b511-cc4e-4557-a1da-184df20fb45f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__guestFont__OzbJ_ color-gc-blue c-active</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/signin</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[2]/header[@class=&quot;page-header guest navbar navbar-fixed-top index__header__26T6B index__whiteBackground__VyPvM&quot;]/div[@class=&quot;page-header-inner container page-top index__topFixed__2IL1o index__whiteBackground__VyPvM&quot;]/div[@class=&quot;page-top nav hidden-xs hidden-sm index__whiteBackground__VyPvM&quot;]/div[@class=&quot;top-menu&quot;]/ul[@class=&quot;nav navbar-nav pull-right index__nav__WOgeF&quot;]/li[3]/a[@class=&quot;index__guestFont__OzbJ_ color-gc-blue c-active&quot;]</value>
   </webElementProperties>
</WebElementEntity>
