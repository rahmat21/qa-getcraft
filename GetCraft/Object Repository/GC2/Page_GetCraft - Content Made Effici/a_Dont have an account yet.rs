<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Dont have an account yet</name>
   <tag></tag>
   <elementGuidId>c19b2176-2ada-4fe6-b73f-49d993680abc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font-theme</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/signup</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Don't have an account yet?</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/main[@class=&quot;page-content no-space padding-page-content   min-height-guest&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-md-offset-3 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;row&quot;]/form[1]/div[@class=&quot;form-actions noborder text-center margin-top-40&quot;]/div[@class=&quot;margin-top-40&quot;]/a[@class=&quot;font-theme&quot;]</value>
   </webElementProperties>
</WebElementEntity>
