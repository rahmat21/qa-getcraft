<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_I need content</name>
   <tag></tag>
   <elementGuidId>c9aea077-07ed-4fc7-beff-82688bf7067c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>I need content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/main[1]/div[@class=&quot;index__mainContainer__2mphf&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;index__marginTopBox__YFMU3&quot;]/div[@class=&quot;index__noPaddingRight__31Tst col-md-6 col-xs-6&quot;]/a[1]/div[@class=&quot;hidden-xs&quot;]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
