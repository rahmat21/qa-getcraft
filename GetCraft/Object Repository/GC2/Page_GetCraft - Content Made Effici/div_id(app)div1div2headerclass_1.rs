<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_id(app)div1div2headerclass_1</name>
   <tag></tag>
   <elementGuidId>d021b5a8-8179-4970-9188-4593d5a593f9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>katalon-rec_elementInfoDiv</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[2]/header[@class=&quot;page-header navbar navbar-fixed-top index__header__2IQRm index__whiteBackground__3vEO9&quot;]/div[@class=&quot;page-header-inner container&quot;]/div[@class=&quot;page-top index__whiteBackground__3vEO9&quot;]/div[@class=&quot;top-menu&quot;]/div[2]/div[@class=&quot;col-md-4 hidden-xs hidden-sm&quot;]/ul[@class=&quot;nav navbar-nav&quot;]/li[2]/a[@class=&quot;text-uppercase color-warm-grey index__fontNavbar__11pyc&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;katalon-rec_elementInfoDiv&quot;)</value>
   </webElementProperties>
</WebElementEntity>
