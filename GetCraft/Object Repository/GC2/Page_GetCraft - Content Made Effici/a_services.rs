<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_services</name>
   <tag></tag>
   <elementGuidId>aa7498e6-43e6-4940-b37f-b50a17ba9d89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/getcraft_id/product' and (text() = 'My Services' or . = 'My Services')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__colorGreyBrown__3zQTl index__customPadding__2Hb4Q</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/getcraft_id/product</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Services</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[2]/header[@class=&quot;page-header navbar navbar-fixed-top index__header__2IQRm index__whiteBackground__3vEO9&quot;]/div[@class=&quot;page-header-inner container&quot;]/div[@class=&quot;page-top index__whiteBackground__3vEO9&quot;]/div[@class=&quot;top-menu&quot;]/div[2]/div[@class=&quot;col-md-4 hidden-xs hidden-sm&quot;]/ul[@class=&quot;nav navbar-nav&quot;]/li[1]/ul[@class=&quot;nav navbar-nav pull-left&quot;]/li[@class=&quot;dropdown dropdown-profile undefined&quot;]/div[@class=&quot;dropdown open btn-group&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[2]/a[@class=&quot;index__colorGreyBrown__3zQTl index__customPadding__2Hb4Q&quot;]</value>
   </webElementProperties>
</WebElementEntity>
