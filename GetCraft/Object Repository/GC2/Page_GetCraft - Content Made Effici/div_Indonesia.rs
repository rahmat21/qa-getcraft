<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Indonesia</name>
   <tag></tag>
   <elementGuidId>39ec7c85-c9de-42b4-8aef-615037f8d372</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Indonesia</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid&quot;]/div[5]/div[1]/div[1]/div[1]/div[1]/span[@class=&quot;text-capitalize&quot;]/div[1]/div[1]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
