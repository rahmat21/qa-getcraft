<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sign in</name>
   <tag></tag>
   <elementGuidId>81e37cef-d98d-47ac-ad29-cb1dad3a7681</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/main[@class=&quot;page-content no-space padding-page-content   min-height-guest&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 col-md-offset-3 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;row&quot;]/form[1]/div[@class=&quot;form-actions noborder text-center margin-top-40&quot;]/div[@class=&quot;margin-top-40&quot;]/div[1]/button[1]/div[1]/div[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
