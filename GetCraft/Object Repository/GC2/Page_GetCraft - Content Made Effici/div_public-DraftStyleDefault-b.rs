<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_public-DraftStyleDefault-b</name>
   <tag></tag>
   <elementGuidId>b0ee9a88-a27e-4d97-8fd1-3e3fa154df60</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-offset-key</name>
      <type>Main</type>
      <value>7mm5h-0-0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>public-DraftStyleDefault-block public-DraftStyleDefault-ltr</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__layout__1pzQY&quot;]/main[@class=&quot;page-content no-space padding-page-content  bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-md-offset-0 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;col-md-8 hidden-xs no-padding-stack&quot;]/form[1]/div[@class=&quot;form-group margin-top-l&quot;]/div[@class=&quot;row no-margin&quot;]/div[@class=&quot;col-md-12 index__fieldPaddingLeft__2qIUx padding-right-30&quot;]/div[1]/div[@class=&quot;RichTextEditor__root___2QXK- index__placeholderColor__2zlil index__ideaBox__2tS6c index__errorBorder__2ZUUg&quot;]/div[@class=&quot;RichTextEditor__editor___1QqIU&quot;]/div[@class=&quot;DraftEditor-root&quot;]/div[@class=&quot;DraftEditor-editorContainer&quot;]/div[@class=&quot;notranslate public-DraftEditor-content&quot;]/div[1]/div[@class=&quot;RichTextEditor__block___2Vs_D RichTextEditor__paragraph___3NTf9&quot;]/div[@class=&quot;public-DraftStyleDefault-block public-DraftStyleDefault-ltr&quot;]</value>
   </webElementProperties>
</WebElementEntity>
