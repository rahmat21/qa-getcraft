<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Influencer Marketing</name>
   <tag></tag>
   <elementGuidId>98a64b79-719d-4dac-bd9b-0c9024d1847b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Influencer Marketing</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__categoryContainer__172xo&quot;]/section[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;hidden-xs margin-top-60&quot;]/div[@class=&quot;col-md-offset-1 col-md-12 col-xs-12 no-padding-stack index__boxPadding__1UBJI&quot;]/div[@class=&quot;col-md-4 col-xs-6 text-center index__itemPadding__2R4Ru&quot;]/a[@class=&quot;index__border__hkwDk&quot;]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
