<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Upload Reference</name>
   <tag></tag>
   <elementGuidId>cd3ee816-14a7-4d95-ac6b-65c7d92d5ec6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class = 'mdi mdi-cloud-upload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdi mdi-cloud-upload</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Upload Reference</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__layout__1pzQY&quot;]/main[@class=&quot;page-content no-space padding-page-content  bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-md-offset-0 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;col-md-8 hidden-xs no-padding-stack&quot;]/form[1]/div[@class=&quot;form-group margin-top-l&quot;]/div[@class=&quot;row no-margin&quot;]/div[@class=&quot;col-md-12 index__fieldPaddingLeft__2qIUx padding-right-30&quot;]/div[@class=&quot;clearfix index__wrapInput__sZXPt&quot;]/div[@class=&quot;clearfix index__customRow__A5-ne&quot;]/div[@class=&quot;col-md-3&quot;]/div[@class=&quot;index__dropzone__3nNRX&quot;]</value>
   </webElementProperties>
</WebElementEntity>
