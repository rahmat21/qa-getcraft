<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Review and request a qu</name>
   <tag></tag>
   <elementGuidId>179ebc81-62e7-4f36-aa19-665cccc136b6</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Review and request a quote</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[1]/div[@class=&quot;index__subHeaderFixed__2zrjw index__subHeaderFixed__2zrjw&quot;]/div[@class=&quot;container no-padding-xs&quot;]/div[@class=&quot;col-md-12 col-xs-12 col-sm-12 no-space&quot;]/div[@class=&quot;col-md-3 hidden-xs hidden-sm no-space&quot;]/nav[@class=&quot;index__requestQuoteButton__30q91&quot;]/div[1]/button[1]</value>
   </webElementProperties>
</WebElementEntity>
