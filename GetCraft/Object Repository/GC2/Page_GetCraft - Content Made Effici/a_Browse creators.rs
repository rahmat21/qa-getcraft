<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Browse creators</name>
   <tag></tag>
   <elementGuidId>02dd8ba2-debe-4454-9c9c-e130955d1c38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(text() = 'Browse creators' or . = 'Browse creators')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__headerLeft__3ure0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/demo_creator2/creator-category</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Browse creators</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[2]/header[@class=&quot;page-header navbar navbar-fixed-top index__header__2IQRm index__whiteBackground__3vEO9&quot;]/div[@class=&quot;page-header-inner container&quot;]/div[@class=&quot;page-top index__whiteBackground__3vEO9&quot;]/div[@class=&quot;top-menu&quot;]/div[@class=&quot;hidden-xs&quot;]/ul[@class=&quot;nav navbar-nav pull-left hidden-xs&quot;]/li[1]/a[@class=&quot;index__headerLeft__3ure0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
