<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>start_project</name>
   <tag></tag>
   <elementGuidId>7babe774-82a7-4979-9129-5aedbfd6c21f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/div/form/div/div[2]/div[3]/div[3]/div[1]/div[1]/div/div/div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'Start Project') or contains(., 'Start Project'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start Project</value>
   </webElementProperties>
</WebElementEntity>
