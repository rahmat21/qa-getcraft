<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_quotation</name>
   <tag></tag>
   <elementGuidId>ee50aade-9397-4a71-bcf7-17b336c989a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[1]/div/div[4]/div/main/section/div/div/div/form/div/div[2]/div[3]/div[3]/div[1]/div[1]/div/div/div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'Approve quotation') or contains(., 'Approve quotation'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Approve quotation</value>
   </webElementProperties>
</WebElementEntity>
