<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_verifikasi</name>
   <tag></tag>
   <elementGuidId>04a69682-b7fb-463e-8cbc-5b18e440cf96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ContentPlaceHolder2_lblMessage&quot;]/table/tbody/tr[2]/td/table/tbody/tr[7]/td/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'http://staging.getcraft.io/') or contains(., 'http://staging.getcraft.io/'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>http://staging.getcraft.io/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>mandrillapp.com</value>
   </webElementProperties>
</WebElementEntity>
