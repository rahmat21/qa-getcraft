<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>payment_100 Upfront</name>
   <tag></tag>
   <elementGuidId>0ac3f523-1882-4587-a7b7-a5b565c7f91b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = '100% upfront' or . = '100% upfront')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>100% upfront</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid&quot;]/div[9]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[1]/div[1]</value>
   </webElementProperties>
</WebElementEntity>
