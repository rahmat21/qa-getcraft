<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Selectall_industry</name>
   <tag></tag>
   <elementGuidId>d45fa081-2a6c-42f5-abf1-3167d5ef9944</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[7]/div/div/div/div[2]/div/input
</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'Select All' or . = 'Select All')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select All</value>
   </webElementProperties>
</WebElementEntity>
