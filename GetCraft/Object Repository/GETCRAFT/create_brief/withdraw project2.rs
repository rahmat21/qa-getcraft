<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>withdraw project2</name>
   <tag></tag>
   <elementGuidId>a21eb3df-9ddf-4edc-a574-3fe0f6a932bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[7]/div/div[1]/div/div/div[2]/button[2]
/html/body/div[8]/div/div[1]/div/div/div[2]/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = 'Withdraw' or . = 'Withdraw')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>margin-bottom-s index__projectName</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Withdraw</value>
   </webElementProperties>
</WebElementEntity>
