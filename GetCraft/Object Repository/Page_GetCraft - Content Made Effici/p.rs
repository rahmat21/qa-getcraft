<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p</name>
   <tag></tag>
   <elementGuidId>78f8e2b0-176b-475e-9427-28230073e8e8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__layout__vk_pN&quot;]/main[@class=&quot;page-content no-space padding-page-content  bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn undefined&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-md-offset-0 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;col-md-8 hidden-xs no-padding-stack&quot;]/form[1]/div[@class=&quot;form-group margin-top-l&quot;]/div[@class=&quot;row no-margin&quot;]/div[@class=&quot;col-md-12 index__fieldPaddingLeft__1W0AM padding-right-30&quot;]/div[1]/div[@class=&quot;quill&quot;]/div[@class=&quot;index__textArea__3wGqe ql-container ql-snow&quot;]/div[@class=&quot;ql-editor ql-blank&quot;]/p[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='app']/div/div[4]/div/div/main/section/div/div/div/form/div[9]/div/div/div/div/div[2]/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Project description'])[1]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This project for..'])[1]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Required'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Content quantity?'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div/div[2]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
