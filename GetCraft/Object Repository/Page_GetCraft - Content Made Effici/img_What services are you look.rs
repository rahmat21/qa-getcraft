<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_What services are you look</name>
   <tag></tag>
   <elementGuidId>ea0e494d-4cca-4401-ae4f-a1bec9777524</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://assets.getcraft.com/images/asset-q2-2018/service-selection/ic_photographers_unselected/ic-photographers_unselected.png</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Photography services</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>index__serviceTypeImage__1BDj0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;page-container bg-white&quot;]/div[@class=&quot;page-content-wrapper&quot;]/div[@class=&quot;index__layout__vk_pN&quot;]/main[@class=&quot;page-content no-space padding-page-content  bgColor-page-content min-height&quot;]/section[@class=&quot;container undefined index__MarginTop__11LJn undefined&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12 col-md-offset-0 col-xs-12 index__withoutPadding__2-yAA&quot;]/div[@class=&quot;col-md-8 hidden-xs no-padding-stack&quot;]/form[1]/div[@class=&quot;col-md-12 col-xs-12 no-padding-stack index__boxPadding__2o6SC&quot;]/div[@class=&quot;col-md-4 col-xs-6 text-center index__boxItem__1XocD&quot;]/img[@class=&quot;index__serviceTypeImage__1BDj0&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='app']/div/div[4]/div/div/main/section/div/div/div/form/div[2]/div[5]/img</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What services are you looking for?'])[1]/following::img[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sub-service'])[1]/preceding::img[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select specific field or format'])[1]/preceding::img[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <value>//img[@alt='Photography services']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[5]/img</value>
   </webElementXpaths>
</WebElementEntity>
