<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_httpstaging.getcraft.iove</name>
   <tag></tag>
   <elementGuidId>25e2065f-756c-4b16-a805-ac0834a6ad40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(contains(text(), 'http://staging.getcraft.io/verify/') or contains(., 'http://staging.getcraft.io/verify/'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/div[3]/div[2]/table/tbody/tr/td/center/table/tbody/tr/td/div/p[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>http://staging.getcraft.io/verify/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/table[@class=&quot;body&quot;]/tbody[1]/tr[1]/td[@class=&quot;center&quot;]/center[1]/table[@class=&quot;container&quot;]/tbody[1]/tr[1]/td[1]/div[@class=&quot;content&quot;]/p[3]/a[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
